# Hibiscus Apis Automated ReST API Tester
---
## Kegunaan
---
Project ini digunakan untuk menguji ReST API



## Nama
---
Apis adalah nama ilmiah untuk lebah





## Kebutuhan
---
Project ini membutuhkan NodeJS dan ExpressJS



# Instalasi
---
`npm install`



`npm run apis`



## Memulai Pengujian
---
1. Edit file `apis.js`
2. Anda bisa mengubah variable untuk menampung instance dari `Sting`
3. Method yang tersedia `get()` dan `post()`




```javascript
import Sting from "./src/sting.js"
let apis = new Sting(hostname)
apis.get(url[,header])
apis.post(url[,formdata[,header]])
```  



`hostname` misalnya `http://localhost/project`




`url` misalnya `/api/class/method`




untuk `formdata` dan `header` gunakan

```javascript
let header = new Headers()
header.append(name,value)

let formdata = new FormData()
formdata.append(name,value)
```




## Perlu Dikerjakan
---
Issue: URL yang di-request lalu melakukan redirect akan menyebabkan error `CORS blocked` 




Issue: ReST Server yang tidak mengaktifkan header `Access-Control-Allow-Origin` dan `Access-Control-Allow-Headers` akan menyebabkan error `CORS blocked`



Solusi: perlu menanam script untuk mem-bypass error tersebut



Issue: support untuk method lain seperti PUT, PATCH, DELETE



Solusi: perlu merubah layout class `Sting` yang memungkinkan sembarang HTTP Method untuk diproses tanpa perlu membuat function khusus



Issue: error sesaat sebelum menekan Ctrl + C pada waktu mematikan NodeJS server



Solusi: ??



`Catatan`
untuk keperluan debugging project, issue di atas bisa di-bypass dengan memasang Chrome Extension CORS unblocker