import { jsonTree } from "../jsonTree/libs/jsonTree/jsonTree.js";

/**
 * Hibiscus Apis The ReST Client
 * Developed by @hibiscusht
 */

class Sting { 
    
    start = Date.now();
    end = 0;
    elapsed = 0;
    index = 0;
    finalResult = "";
    elems = document.querySelector(".result");
    stats = document.querySelector(".stats");
    url;
    failed = 0;
    passed = 0;
    tested = 0;


    checkElem(){
         let unexpected = this.elems ?? "cannot find target result. you must add a DIV tag with class='result'";
         console.log(unexpected);
         let status = this.elems == null ? false : true;
         return status;
    }


    constructor(hostname){
        this.checkElem();
        this.url = hostname;
    }
  


    /*
    doing the test
    first argument = mode
    second argument = url to test
    third argument = header
    or fourth argument = form data 
    */
    async test(mode,url,header,formdata){ 
        this.tested += 1; 
        let dt = "";

        try{
      
       if(mode == "GET"){
        dt = await fetch(url,{
            headers: header,
            redirect: "follow"
        });
       } else {
        dt = await fetch(url,{
            method: mode,
            headers: header,
            body: formdata,
            redirect: "follow" 
        });
       }
       let dta = await dt.json();
       if(dt.ok){

           this.index = Math.ceil(Math.random() * 100000);
           this.finalResult = "<hr style='border: dotted 1px black'>"+ mode + " (<span style='background: green; color: white'><strong>" + dt.status + " " + dt.statusText  + "</strong></span>) " +  url + "<br/><div id='result-" + this.index + "'></div>";   
           this.elems.innerHTML += this.finalResult;
           let tree = jsonTree.create(dta,document.getElementById("result-" + this.index));
            tree.expand();
            this.passed += 1;

       } else {
           
           this.index = Math.ceil(Math.random() * 100000);
           this.finalResult = "<hr style='border: dotted 1px black'>"+ mode + " (<span style='background: red; color: white'><strong>" + dt.status + " " + dt.statusText  + "</strong></span>) " +  url + "<br/><div id='result-" + this.index + "'></div>";   
           this.elems.innerHTML += this.finalResult;
           let tree = jsonTree.create(dta,document.getElementById("result-" + this.index));
            tree.expand();
           this.failed += 1;
       }

    } catch(e){
        this.index = Math.ceil(Math.random() * 100000);
           this.finalResult = "<hr style='border: dotted 1px black'>"+ mode + " (<span style='background: red; color: white'><strong>FAILED</strong></span>) " +  url + "<br/><div id='result-" + this.index + "'>"+ e  + "</div>";   
           this.elems.innerHTML += this.finalResult;
           this.failed += 1;
    }
          
    }

    statistics(){
        this.stats.innerHTML = "<hr style='border: dotted 1px black'>" + this.tested + " request(s) completed. " + this.passed + " request(s) passed " + this.failed + " request(s) failed . all request(s) completed in " + this.elapsed + " second(s)";
    }
     
  async get(url,...any){ 
        let uri = this.url + url;
        let mode = "GET";
        let header = any[0] ?? {};
        let formdata = {};

  
        await this.test(mode,uri,header,formdata);
        
            this.end = Date.now();
            let diff = (this.end - this.start) / 1000;
            this.elapsed += diff;
            this.elems.innerHTML += "request completed in " + diff + " second(s)";

            this.statistics();   

    }

    
  async post(url,...any){ 
    let uri = this.url + url;
    let mode = "POST";
    let header = any[1] ?? {};
    let formdata = any[0] ?? {};


    await this.test(mode,uri,header,formdata);
    
        this.end = Date.now();
        let diff = (this.end - this.start) / 1000;
        this.elapsed += diff;
        this.elems.innerHTML += "request completed in " + diff + " second(s)";

        this.statistics();   

}
}

export default Sting;